// xls2xml project xls2xml.go
package main

// F:\Go\local\bin>xls2xml.exe -s "./SHW.xls" -dt "./txt"

import (
	"archive/zip"
	"bufio"
	"bytes"
	"code.google.com/p/go.text/encoding/simplifiedchinese"
	"code.google.com/p/go.text/transform"
	_ "encoding/csv"
	"encoding/xml"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	_ "reflect"
	"runtime"
	"strconv"
	"strings"
	"sync"

	//"xls2xml/set"
	_ "unsafe"
)

var (
	srcXsl = flag.String("s", "./xls/SHW.xls", "the src xls path")
	dirTxt = flag.String("dt", "./dst/txt", "the dst txts directory")
	dstZip = flag.String("dz", "./dst/xml/Setting_zh-Hans.zip", "the dst zip's path")
	delim  = flag.String("del", "/", "the delimiter")

	conf = "./xls/Default.x2c"
	//localConf = "./conf.txt"
	g_version = 1

	g_config  Config
	g_dirCsv  string = "./csv"
	g_xmlPath string

	g_stopWords   stopWords
	stopWordsPath = "./xls/stopwords.txt"

	g_subDirectorys = []string{"humandata", "local", "Market", "script"}

	//copy到子目录
	//key是被copy成的文件的相对于*dirTxt的路径
	//val是由xls2csv生成的文件的最终文件名
	g_csvTxt = map[string]string{
		"humandata/warrior.csv": "humandata.csv",
	}
)

/*
<Config>
	<AppRes Ip="192.168.1.70" Port="10000" Url="http://192.168.1.70/res/" Version="1.5.1.0" />
	<Update SettingVer="128" LocalVer="116" HelpVer="108"/>
</Config>

*/

//======================================================================
//Config.xml
//======================================================================
type Config struct {
	XMLName xml.Name `xml:"Config"` // 一般建议根元素加上此字段
	AppRes  AppRes   `xml:"AppRes"`
	Update  Update   `xml:"Update"`
}

type AppRes struct {
	Ip      string `xml:",attr"`
	Port    string `xml:",attr"`
	Url     string `xml:",attr"`
	version string `xml:",attr"`
}

type Update struct {
	SettingVer string `xml:",attr"`
	LocalVer   string `xml:",attr"`
	HelpVer    string `xml:",attr"`
}

//======================================================================

//======================================================================
// Stop Words
//======================================================================
type stopWords struct {
	set map[string]bool
}

func (set *stopWords) add(i string) bool {
	if set.set == nil {
		set.set = make(map[string]bool)
	}

	i = strings.TrimRight(i, "\r\n ")
	i = strings.ToLower(i)
	_, found := set.set[i]
	set.set[i] = true
	return !found //False if it existed already
}

func (set *stopWords) del(i string) {
	delete(set.set, i)
}

func (set *stopWords) isExist(key string) bool {
	key = strings.ToLower(key)
	_, found := set.set[key]

	return found
}

func (set *stopWords) output() {
	for key, _ := range set.set {
		fmt.Println(key)
	}
}

//========================================================================

//转化成csv文件
func xls2Csv() error {

	fmt.Println("xls2csv is starting, pls waiting")
	c := exec.Command("xls2csv", *srcXsl, g_dirCsv, conf)

	if err := c.Run(); err != nil {
		fmt.Println("xls2csv is failed with ERROR: ", err)
		return err
	} else {
		fmt.Println("xls2csv is finished")
		return nil
	}
}

func allFiles(dir, prefix string) ([]string, error) {

	files := make([]string, 0)

	err := filepath.Walk(dir, func(path string, f os.FileInfo, err error) error {
		if f == nil {
			return err
		}

		if f.IsDir() {
			return nil
		}

		//fmt.Println(path)
		if !strings.HasSuffix(path, prefix) {
			//	fmt.Println("Remove: ", path)
			os.Remove(path)

		} else {
			files = append(files, path)
		}

		return nil
	})
	/*
		for _, file := range files {
			fmt.Println(file)
		}
	*/
	return files, err
}

func progress(ch <-chan int, size int) error {

	finished := 1

FOOR_LOOP:
	for {
		select {
		case _, isLive := <-ch:
			{
				if !isLive {
					break FOOR_LOOP
				}
				finished++
				for i := 0; i < finished; i++ {
					//	fmt.Printf("=")
				}

				//fmt.Printf("%d%%\n", int(float32(finished)/float32(size)*100))

			}
		}
	}
	return nil
}

//csv转化成txt
func csv2Txt() error {
	fmt.Println("csv2txt is starting, pls waiting")

	//获得当前目录下的所有文件
	// //	files, err := filepath.Glob(g_dirCsv + "/*.csv")
	files, err := allFiles(g_dirCsv, "csv")
	if err != nil {
		return err
	}

	var wg sync.WaitGroup

	//进度条
	chanProgress := make(chan int)

	go progress(chanProgress, len(files))

	//将文件由.csv修改为.txt
	for _, file := range files {
		//fmt.Println("FILE: ", file)
		wg.Add(1)

		//从新生成文件
		go func(src string, ch chan<- int) {

			genTxt(src)

			ch <- 1

			wg.Done()
		}(file, chanProgress)
	}

	wg.Wait()

	close(chanProgress)

	return nil
}

func genTxt(src string) error {

	srcFile, err := os.OpenFile(src, os.O_RDONLY, 0666)
	if err != nil {
		fmt.Printf("openFile: %s is failed ", src)
		return err
	}
	defer srcFile.Close()

	srcReader := bufio.NewReader(srcFile)

	src = filepath.Base(src)
	dst := strings.Replace(src, ".csv", ".txt", -1) //csv ->txt

	temp := *srcXsl
	temp = filepath.Base(temp)
	temp = strings.TrimSuffix(temp, filepath.Ext(temp)) + "_" //SHW_XX ->XX
	dst = strings.TrimPrefix(dst, temp)
	dst = strings.ToLower(dst)

	//int index = strings.Index(dst, "_") //去首
	//dst = dst[index+1]

	dst = *dirTxt + "/" + dst
	dst = filepath.Clean(dst)

	//fmt.Printf("gen %s is started\n", dst)

	//	dstFile, err := os.OpenFile(dst, os.O_WRONLY|os.O_CREATE, 0666)
	dstFile, err := os.Create(dst)

	if err != nil {
		fmt.Printf("openFile: %s is failed ", dst)
		return err
	}

	defer dstFile.Close()
	//dstFile.WriteString("\xEF\xBB\xBF")
	for {
		str, err := srcReader.ReadString('\n')

		if err != nil {
			if err == io.EOF {
				//fmt.Println("The file end is touched.", str)

				//处理最后一行
				str = strings.Replace(str, ",", *delim, -1)
				_, err = dstFile.WriteString(str + "\n")
				if err != nil {
					fmt.Println("Write failed with error: ", err)
				}

				break
			} else {
				fmt.Println("gen failed is failed with err: ", err)
				return err
			}
		} // end of if err != nil

		//跳过空行
		if 0 == len(str) || str == "\r\n" {
			continue
		}

		//跳过#号开始的的行
		str = strings.TrimSpace(str)
		if str[0] == '#' {
			continue
		}

		//完成替换
		str = strings.Replace(str, ",", *delim, -1)
		_, err = dstFile.WriteString(str + "\n")
		if err != nil {
			fmt.Println("Write failed with error: ", err)
		}

	} // end of for {}

	//刷空
	dstFile.Sync()
	//fmt.Printf("gen %s is finished\n", dst)
	return nil

}

func writeHeader(f *os.File) error {

	f.WriteString(fmt.Sprintf("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n"+
		"<root version=\"%d\">\n", g_version))
	return nil

}

func writeFooter(f *os.File) error {
	f.WriteString("</root>")
	return nil
}

func writeBody(f *os.File) error {

	files, err := allFiles(*dirTxt, ".txt")
	if err != nil {
		return err
	}

	for _, file := range files {

		base := filepath.Base(file)
		base = strings.TrimSuffix(base, filepath.Ext(base))

		//过滤
		if !g_stopWords.isExist(base) {

			continue
		}

		//打开源文件
		srcFile, err := os.Open(file)

		if err != nil {
			continue
		}
		defer srcFile.Close()

		//段头
		f.WriteString(fmt.Sprintf("<%s>\n", base))

		body, _ := ioutil.ReadAll(srcFile)

		body, _ = decode(body)

		f.Write(body)
		//段体
		//io.Copy(f, srcFile)

		//段尾
		f.WriteString(fmt.Sprintf("</%s>\n", base))
	}

	return nil
}

// utf8->gbk
func encode(str []byte) ([]byte, error) {
	str, err := ioutil.ReadAll(transform.NewReader(bytes.NewReader(str), simplifiedchinese.GBK.NewEncoder()))
	if err != nil {
		return nil, err
	}
	return str, nil
}

// gbk->utf8
func decode(str []byte) ([]byte, error) {
	str, err := ioutil.ReadAll(transform.NewReader(bytes.NewReader(str), simplifiedchinese.GBK.NewDecoder()))
	if err != nil {
		return nil, err
	}

	return str, nil
}
func txt2Xml() error {

	//path := strings.Replace(*dstZip, ".zip", ".xml", 1)
	//xmlFile, err := os.OpenFile(g_xmlPath, os.O_WRONLY|os.O_CREATE, 0666)
	xmlFile, err := os.Create(g_xmlPath)

	if err != nil {
		fmt.Printf("openFile: %s is failed with error:%s ", *dstZip, err)
		return err
	}

	defer xmlFile.Close()

	//写头
	writeHeader(xmlFile)

	//写体
	writeBody(xmlFile)

	//写尾
	writeFooter(xmlFile)

	xmlFile.Sync()

	return nil
}

// http://my.oschina.net/chai2010/blog/186211
func xml2Zip() error {

	//file, err := os.OpenFile(*dstZip, os.O_WRONLY|os.O_CREATE, 0666)
	file, err := os.Create(*dstZip)
	if err != nil {
		return err
	}
	defer file.Close()
	wzip := zip.NewWriter(file)
	defer func() error {
		if err := wzip.Close(); err != nil {
			return err
		}
		return nil
	}()

	base := filepath.Base(g_xmlPath)

	f, err := wzip.Create(base)
	if err != nil {
		return err
	}

	data, err := ioutil.ReadFile(g_xmlPath)
	if err != nil {
		return err
	}

	_, err = f.Write(data)
	return err
}

func work() error {

	var err error
	defer func() error {
		if e := recover(); e != nil {
			fmt.Println(err)
		}
		return err
	}()

	if err = xls2Csv(); err != nil {
		return err
	}

	if err = csv2Txt(); err != nil {
		return err
	}

	if err = txt2Xml(); err != nil {
		return err
	}

	if err = xml2Zip(); err != nil {
		return err
	}
	utils()
	return clean()

}

func isExist(path string) bool {
	_, err := os.Stat(path)

	return err == nil || os.IsExist(err)
}

func mkdir() error {
	var err error

	//创建文本所在目录
	if !isExist(*dirTxt) {
		if err = os.MkdirAll(*dirTxt, os.ModePerm); err != nil {
			return err
		}
	}

	//创建文本所在目录的子目录

	for _, subDir := range g_subDirectorys {
		path := *dirTxt + "/" + subDir
		if !isExist(path) {
			if err = os.MkdirAll(path, os.ModePerm); err != nil {
				fmt.Printf("create dir: %s is failed\n", path)
				continue
			}
		}
	}

	dirZip := filepath.Dir(*dstZip)
	//创建zip所在目录
	if !isExist(dirZip) {
		if err = os.MkdirAll(dirZip, os.ModePerm); err != nil {
			return err
		}
	}
	return nil
}
func parse() error {
	flag.Usage()
	flag.Parse()

	fmt.Printf("\nSRC: %s\nDstTxt: %s\nDstTxt: %s\nDELIM: %s", *srcXsl, *dirTxt, *dstZip, *delim)

	g_xmlPath = strings.Replace(*dstZip, ".zip", ".xml", 1)

	genStopWordsSet()
	version()

	return mkdir()
}

func unmarshal() error {

	path := filepath.Dir(*dstZip) + "/Config.xml"
	//fmt.Println("config's path: ", path)
	content, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}

	err = xml.Unmarshal(content, &g_config)
	if err != nil {
		return err
	}

	g_version, err = strconv.Atoi(g_config.Update.SettingVer)
	//fmt.Println(g_config)
	return err

}

func marshal() error {
	g_version++
	g_config.Update.SettingVer = strconv.Itoa(g_version)

	output, err := xml.MarshalIndent(g_config, "  ", "    ")
	if err != nil {
		fmt.Printf("error: %v\n", err)
	}

	path := filepath.Dir(*dstZip) + "/Config.xml"
	f, err := os.Create(path)
	if err != nil {
		return err
	}

	defer f.Close()
	f.Write([]byte(xml.Header))
	f.Write(output)
	//os.Stdout.Write(output)
	//os.Stdout.Write([]byte(xml.Header))
	return nil
}

func genStopWordsSet() error {

	f, err := os.OpenFile(stopWordsPath, os.O_RDONLY, 0666)
	if err != nil {
		fmt.Printf("openFile: %s is failed ", stopWordsPath)
		return err
	}

	defer f.Close()
	srcReader := bufio.NewReader(f)
	for {
		line, err := srcReader.ReadString('\n')
		//fmt.Println(err, "line: ", line)

		if err != nil {
			if err == io.EOF {
				//fmt.Println("The file end is touched.")
				break
			} else {
				fmt.Println("gen failed is failed with err: ", err)
				return err
			}
		} //
		//跳过空行
		if 0 == len(line) || line == "\r\n" {
			continue
		}

		g_stopWords.add(line)
	} // end of for {}

	//g_stopWords.output()

	return nil

}

/*
// [gocn:10630] 怎样在unsafe.Pointer与slice之间交换数据
// http://blog.rqhut.net/article/%5Bgolang%5D%E6%97%A0%E5%A4%8D%E5%88%B6%E5%B0%86%E5%AD%97%E7%AC%A6%E4%B8%B2%E8%BD%AC%E6%8D%A2%E4%B8%BA%5B%5Dbyte
func version() error {

	var confFile *os.File
	var err error
	var mode = os.O_RDWR

	if !isExist(localConf) {
		mode = mode | os.O_CREATE
	}

	confFile, err = os.OpenFile(localConf, mode, 0666)
	if err != nil {
		fmt.Printf("openFile: %s is failed ", localConf)
		return err
	}
	defer confFile.Close()

	var buffer []byte
	h := (*reflect.SliceHeader)((unsafe.Pointer(&buffer)))
	h.Data = uintptr(unsafe.Pointer(&g_version))
	h.Cap = int(unsafe.Sizeof(g_version))
	h.Len = h.Cap

	_, err = confFile.ReadAt(buffer, 0)
	if err != nil && err != io.EOF {
		return err
	}
	g_version++
	//fmt.Println("\nVersion is : ", g_version)
	fmt.Println(buffer)

	confFile.WriteAt(buffer, 0)
	confFile.Sync()

	g_config.Update.SettingVer = strconv.Itoa(g_version)
	marshal()

	return nil
}
*/

func version() error {
	var err error
	if err = unmarshal(); err != nil {
		return err
	}

	err = marshal()
	return err
}

//杂项
func utils() error {
	for k, v := range g_csvTxt {
		subDir := filepath.Dir(k)
		subDir = *dirTxt + "/" + subDir

		//不存在此子目录,先创建
		if !isExist(subDir) {
			if err := os.MkdirAll(subDir, os.ModePerm); err != nil {
				fmt.Printf("create dir: %s is failed\n", subDir)
				continue
			}
		}

		dstFile, err := os.Create(*dirTxt + "/" + k)
		if err != nil {
			continue
		}
		defer dstFile.Close()

		temp := filepath.Base(*srcXsl)
		temp = strings.TrimSuffix(temp, filepath.Ext(temp)) //xx ->SHW_XX
		srcPath := g_dirCsv + "/" + temp + "_" + v
		srcFile, err := os.Open(srcPath)
		if err != nil {
			continue
		}
		defer srcFile.Close()

		io.Copy(dstFile, srcFile)

	}
	return nil
}

func clean() error {
	return nil
	//删除csv文件夹
	//os.RemoveAll(g_dirCsv)
	//删除xml
	os.Remove(g_xmlPath)
	return nil
}

func main() {

	//开启多核支持
	runtime.GOMAXPROCS(runtime.NumCPU())

	if err := parse(); err != nil {
		fmt.Println(err)
	}
	work()

}
